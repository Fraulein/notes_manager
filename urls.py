from django.conf.urls import patterns, include, url
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'manager_app.views.home', name='home'),
    url(r'^notes/$', 'manager_app.views.notes'),

    url(r'^note/(?P<note_uuid>\w+)/$', 'manager_app.views.note'),
    url(r'^note/$', 'manager_app.views.note'),

    url(r'^categories/$', 'manager_app.views.categories'),
#    url(r'^logout/', 'manager_app.views.logout_view'),
    (r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    (r'^register/$', 'manager_app.views.register'),
    (r'^accounts/login/$', 'django.contrib.auth.views.login'),
#    (r'^accounts/login/$',  login),
#    (r'^accounts/logout/$', logout),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
